# The Legend of Heroes: Trails of Zero #

Game The Legend of Heroes: Trails of Zero PC Version, English patched using Geofront v1.0

### Installation Guide  ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Sources ###

* Game: https://nblog.org/pc-games/the-legend-of-heroes-zero-no-kiseki-english-patched-pc/
* Patch: https://geofront.esterior.net/downloads/
